import React from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import Navigate from '../Navigation';
import SignUpPage from '../SignUp/signUp';
import SignUpAdmin from '../SignUp/SignUp Admin/SignUpT';
import SignUpLearner from '../SignUp/SignUp Learner/SignUp';
import SignInPage from '../SignIn';
import TopicForum from '../Topics Forum/TopicForum';
import HomePage from '../Home/homePage';
import ProfilePage from '../Profile/Profile';
import LearnerProfile from '../Profile/ProfileL';
import * as ROUTES from '../../constants/routes';
import LearnerSignInPage from '../LearnerSignIn/LearnerSignIn';
import AdminSignInPage from '../AdminSignIn/AdminSignIn';
import './app.css';
import PersonalDetailsPage from '../PersonalDetails/PersonalDetails';
import SubjectsPage from '../Subjects/Subjects';
import MathPage from '../Math/Math';
import GradebookPage from '../Gradebook/Gradebook';
import AssignmentPage from '../Assignments/Assignments';
import ResourcesPage from '../Resources/Resources';

const App = () => (
  <Router>
      <div className={'container-all'}>
        <Navigate />

        <hr/>

        <Route path={ROUTES.SIGN_UP} component={SignUpPage} />
        <Route path={ROUTES.SIGN_IN} component={SignInPage} />
        <Route exact path={ROUTES.TOPICS_FORUM} component={TopicForum} />
        <Route exact path={ROUTES.TOPICS_FORUM_ADD} component={TopicForum} />
        <Route path={ROUTES.HOME} component={HomePage} />
        <Route path={ROUTES.PROFILE} component={ProfilePage} />
        <Route path={ROUTES.LEARNER_PROFILE} component={LearnerProfile} />
        <Route path={ROUTES.LEARNER_SIGN_IN} component={LearnerSignInPage} />
        <Route path={ROUTES.ADMIN_SIGN_IN} component={AdminSignInPage} />
        <Route path={ROUTES.SIGN_ADMIN} component={SignUpAdmin}/>
        <Route path={ROUTES.SIGN_LEARNER} component={SignUpLearner}/>
        <Route path={ROUTES.PERSONAL_DETAILS} component={PersonalDetailsPage} />
        <Route path={ROUTES.SUBJECTS} component={SubjectsPage} />
        <Route path={ROUTES.MATH} component={MathPage} />
        <Route path={ROUTES.GRADEBOOK} component={GradebookPage} />
        <Route exact path={ROUTES.ASSIGNMENTS} component={AssignmentPage} />
        <Route exact path={ROUTES.ASSIGNMENTS_ADD} component={AssignmentPage} />
        <Route path={ROUTES.RESOURCES} component={ResourcesPage} />

      </div>
  </Router>
);

export default App;