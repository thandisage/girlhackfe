import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

class Resources extends Component{
  
  render() {
    return(
      <ul className={'container'}>
        <li>
          Past Papers
          <ul>
            <li>
              Test2018.pdf
            </li>
            <li>
              Test2017.pdf
            </li>
            <li>
              Test2016.pdf
            </li>
          </ul>
        </li>
        <li>
          Audio
          <ul>
            <li>
              Exponents And Surds.mp3
            </li>
            <li>
              Equations And Inequalities.mp3
            </li>
            <li>
              Number Patterns.mp3
            </li>
            <li>
              Analytical Geometry.mp3
            </li>
          </ul>
        </li>
        <li>
          Textbook
            <ul>
              <li>
                Siyavula Mathematics.pdf
              </li>
              <li>
                Platinum Mathematics.pdf
              </li>
              <li>
                Everything Maths.pdf
              </li>
            </ul>
        </li>
        <li>
          Video 
          <ul>
            <li>
              Exponents And Surds.mp4
            </li>
            <li>
              Equations And Inequalities.mp4
            </li>
            <li>
              Number Patterns.mp4
            </li>
            <li>
              Analytical Geometry.mp4
            </li>
          </ul>
        </li>
        <li>
          Extra Excercise 
          <ul>
            <li>
              Siyavula: Exponents And Surds
            </li>
            <li>
             Platinum: Equations And Inequalities
            </li>
            <li>
              Everything Maths: Number Patterns
            </li>
            <li>
              Everything Maths: Analytical Geometry
            </li>
          </ul>
        </li>
        <div>
        <Link to={ROUTES.MATH}> {"<<"}</Link>
      </div>
      </ul>
      
    )
  }
}

export default Resources;