import React from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

const Math = () => (
  <div>
      <h1>Mathematics</h1>
    <ul>
      <li>
        <Link to={ROUTES.GRADEBOOK}>Gradebook</Link>
      </li>
      <li>
        <Link to={ROUTES.ASSIGNMENTS_ADD}>Assignments</Link>
      </li>
      <li>
        <Link to={ROUTES.TOPICS_FORUM_ADD}>Topic Forum</Link>
      </li>
      <li>
        <Link to={ROUTES.RESOURCES}>Resources</Link>
      </li>
    </ul>
    <Link to={ROUTES.SUBJECTS}> {"<<"}</Link>
  </div>
);

export default Math;