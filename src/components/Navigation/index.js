import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import './nav.css';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', 
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    backgroundColor: 'grey',
    display: 'flex',
    flexDirection: 'column',
  },

});
class Navigate extends Component {

  render(){
    const { classes } = this.props;

    var isLoggedIn = false;
   
    var href = window.location.href;
    if (href.includes('signin')) {
      isLoggedIn = true;
      }

    return(
      <div>
        <main className={classes.main}>
          <CssBaseline />
          <Paper className={classes.paper}>
            <div className={'links'}> 
                {isLoggedIn ? '' : <Link className={'links'} to={ROUTES.SIGN_IN}>Sign In</Link> }
                {isLoggedIn ? '' : <Link className={'links'} to={ROUTES.SIGN_UP}>Sign Up</Link> }
                <Link className={'links'} to={ROUTES.HOME}>Home</Link>      
                {isLoggedIn ? <Link className={'links'} to={ROUTES.PROFILE}>Profile</Link> : '' }
     
          </div>            
          </Paper>
        </main>
      </div>
    )
  }
};

Navigate.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navigate);