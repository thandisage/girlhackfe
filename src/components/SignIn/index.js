import React from 'react';
import * as ROUTES from '../../constants/routes';
import {Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import './signIn.css';

const styles = theme => ({
  main: {
    width: 'auto',
    height: 100 + '%',
    display: 'block', 
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 1000,
      height: 100 + '%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: 100,
    height: 100 + '%',
    display: 'flex',
    backgroundColor: 'lightgrey',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },

});

class SignIn extends React.Component{
  constructor() {
    super();
    this.state={
      Redirect: false
    };
  }

  setRedirect1=() =>{
    this.setState({
      Redirect1: true
    })
  }

  setRedirect2=() =>{
    this.setState({
      Redirect2: true
    })
  }
  
 
  render() {
    const { classes } = this.props;

    if(this.state.Redirect1){
      return <Redirect to={ROUTES.LEARNER_SIGN_IN} />
    }

    if(this.state.Redirect2){
      return <Redirect to={ROUTES.ADMIN_SIGN_IN} />
    }
    return(
      <div className={'container'}>
        <main className={classes.main}>
          <CssBaseline />
          <Paper className={classes.paper}>
            <div className={'inner-cont'}>
              <button
                className={'learnerSignIn-holder'}
                type= "button"
                name="SignIn-btn"
                onClick={this.setRedirect1}>
                    Learner
              </button>

              <button
                className={'adminSignIn-holder'}
                type= "button"
                name="SignIn-btn"
                onClick={this.setRedirect2}>
                    Admin
              </button>
            </div>
          </Paper>
        </main>
      </div>
    )
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);