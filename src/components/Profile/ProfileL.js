import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes'

class ProfileL extends Component {
    render(){
        return(
            <div>
                <ul>
                    <li>
                        <Link to={ROUTES.PERSONAL_DETAILS}>Personal Details</Link>
                    </li>
                    <li>
                        <Link to={ROUTES.SUBJECTS}>Subjects</Link>
                    </li>
                </ul>
            </div>
        )
    }
};

export default ProfileL;