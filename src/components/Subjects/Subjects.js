import React from 'react';
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

const Subjects = () => (
  <div>
      <h1>Subjects</h1>
    <ul>
      <li>
        <Link to={ROUTES.MATH}>Mathematics</Link>
      </li>
      <li>
        <p>Physical Sciences</p>
      </li>
      <li>
        <p>Life Sciences</p>
      </li>
      <li>
        <p>English (FAL)</p>
      </li>
      <li>
        <p>SeSwati (Home language)</p>
      </li>
    </ul>
    <Link to={ROUTES.LEARNER_PROFILE}> {"<<"}</Link>
  </div>
);

export default Subjects;