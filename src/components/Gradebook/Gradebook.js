import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import './Gradebook.css';

class Gradebook extends Component{

    constructor(props) {
        super(props) 
        this.state = { 
           Progress: [
              { id: 1, topic: 'Exponents And Surds',  percentage: '85%' },
              { id: 2, topic: 'Equations And Inequalities', percentage: '60%' },
              { id: 3, topic: 'Number Patterns', percentage: '87%' },
              { id: 4, topic: 'Analytical Geometry', percentage: '20%' }
           ]
        }
     }
    
     render() { 
        return (
           <div className={'gradebook'}>
              <h1>Progress</h1>
              <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Topic</th>
                    <th>Percentage</th>
                    </tr>
                </thead>
                <tbody>
                {
                    this.state.Progress.map(function(item){
                        return(<tr key={item.id} >
                            <td >{item.topic}</td>
                            <td >{item.percentage}</td>
                        </tr>)
                    })
                }</tbody>
                </Table>
                <Link to={ROUTES.MATH}> {"<<"}</Link>
           </div>
        )
     }
}

export default Gradebook;