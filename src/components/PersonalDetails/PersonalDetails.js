import React, {Component} from 'react';
import * as ROUTES from '../../constants/routes';
import {Redirect} from 'react-router-dom';

class PersonalDetails extends Component{
    constructor() {
        super();
        this.state={
          Redirect: false,
          school: "",
          grade: "",
          name: "Sibonelo",
          surname: "",
          numbers: "",
          password: "",
          confPass: "",
          showPassword: false
        };

      }
    
      onChange(){

      }


      showPassword=() =>{
        var currentState= this.state.showPassword;
        this.setState({
          showPassword: !currentState
        })
      }
    
      setRedirect=() =>{
        this.setState({
          Redirect: true
        })
      }
    
      render() {
          if(this.state.Redirect){
            return <Redirect to={ROUTES.LEARNER_PROFILE} />
          }
        return(
          <div className={'container'}>
            <div className={'school-holder'}>
              <h1>Learner</h1>
              <p>School</p>
              <input
                type= "text"
                name= "text"
                defaultValue= "Phumulo High School"/>
            </div>

            <div className="grade-holder">
                <p>Grade</p>
                <input
                    type = "text"
                    name = "text"
                    defaultValue = "Grade 11"/>
            </div>
        
            <div className="name-holder">
                <p>Name</p>
                <input
                    type = "text"
                    name = "text"
                    defaultValue = "Sibonelo"/>
            </div>
          
            <div className="surname-holder">
                <p>Surname</p>
                <input
                    type = "text"
                    name = "text"
                    defaultValue = "Mtshali"/>
            </div>

            <div className="numbers-holder">
                <p>Contact Numbers</p>
                <input
                    type = "text"
                    name = "text"
                    defaultValue = "0713568964"/>
            </div>
        
            <div className="password-holder">
                <p>Password</p>
                <input
                    type = "password"
                    name = "text"
                    defaultValue = "Sibonelo21"/>
            </div>

            <div className="confPass-holder">
                <p>Confirm Password</p>
                <input
                    type = {this.state.showPassword? "text":"password"}
                    name = "text"
                    defaultValue = "Sibonelo21"/>
                    <button className={'view-pass'} onClick={this.showPassword}>
                      {this.state.showPassword? "hide":"show"} password
                    </button>
            </div>
        
            <div className={'submit-holder'}>
              <button
                type= "button"
                name="confirm-btn"
                onClick={this.setRedirect}>
                    Confirm
                </button>
            </div>
         
          </div>
        )
      }

}

export default PersonalDetails;