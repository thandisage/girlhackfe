import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import {Redirect} from 'react-router-dom';


class TopicForum extends Component{
  constructor() {
    super();
    this.state={
      Redirect: false
    };
  }

  setRedirect=(e) =>{
    
    // debugger
    var description = document.querySelector("#description").value.trim()
    if(!this.state.Redirect && description !== ''){
      alert("Your work is uploaded!")
      this.setState({
        Redirect: true
      })
    }else{
      alert("Description cannot be empty!");
      this.setState({
        Redirect: false
      })
    }
  }

  render() {
      if(this.state.Redirect){
        return <Redirect to={ROUTES.TOPICS_FORUM} />
      }
    return(
      <div className={'container'}>
        <div className={'description-holder'}>
          <h1>TopicForum</h1>
          <p>Descprition</p>
          <input id='description'
            type= "text"
            name= "text"
            placeholder= "Descprition"/>
        </div>
    
        <div className={'submit-holder'}>
          <button
            type= "button"
            name="Submit-btn"
            onClick={this.setRedirect}>
                Submit
            </button>
        </div>
        <Link to={ROUTES.MATH}> {"<<"}</Link>
      </div>
    )
  }
}

export default TopicForum;