import React from 'react';
import lightBulb from '../Home/LIGHTBULB.jpg';
import tree from '../Home/TREEROOTS.jpg';
import './homePage.css';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
  main: {
    width: 'auto',
    height: 100 + '%',
    display: 'block', 
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 1000,
      height: 100 + '%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    // marginTop: theme.spacing.unit * 8,
    display: 'flex',
    backgroundColor: 'GREY',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },

});

class HomePage extends React.Component {

  render() {
    const { classes } = this.props;
    return(
      <div>
        <main className={classes.main}>
          <CssBaseline />
          <Paper className={classes.paper}>
            <div className={'banner-cont'}>
              <img className={'banner'}  src={lightBulb}/>
            </div>

            <span style={{fontSize:'120px', lineHeight:'0.6em', opacity:0.2}}>❝</span>
            <center>
            <p><i>"Learning is the only thing the mind</i></p>
            <p><i>never exhaughts, never fears</i></p>
            <p className={'statement'}><i>and never regrets" </i></p>

            <div>
              <img className={'treeRoots'} src={tree}/>
            </div>

            <p>One of the inhibitors of equal and quality education is poor
              access to learning material and even the sharing of learning materials like textbooks which with time get worn out. </p>
              <p> This learning platform engages both teacher and learner to promote life-long learning on both
                interacting parties.
              The aim is Promoting improved learning experiences, literacy, learning with understanding  and comprehension.
                We aim to change the traditional method of learning by accommodating most of the learning styles 
                that will suit different learners, in this way it is possible to monitor and track the progress of each learner. The features 
                available for the learners will be used as resources, these resources include past examination papers, 
                textbooks and exercises. 
              
              </p>
            </center>
          </Paper>
        </main>
      </div>
    )
  }
}

HomePage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HomePage);