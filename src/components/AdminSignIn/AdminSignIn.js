import React from 'react';
import * as ROUTES from '../../constants/routes';
import {Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import '../LearnerSignIn/learner.css';

const styles = theme => ({
  main: {
    width: 'auto',
    height: 100 + '%',
    display: 'block', 
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 1000,
      height: 100 + '%',
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    backgroundColor: 'lightgrey',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },

});

class AdminSignIn extends React.Component{
  constructor() {
    super();
    this.state={
      Redirect: false,
      district: "",
      school: "",
      name: "",
      surname: "",
      password: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  setRedirect=() =>{
    this.setState({
      Redirect: true
    })
   }

   handleChange(event){
    
    this.setState({[event.target.name]: event.target.value});
     
   }
  
  render() {
      if(this.state.Redirect){
        return <Redirect to={ROUTES.PROFILE} />
      }
      const { classes } = this.props;

      const isValid= this.state.district !== '' || this.state.school !== '' || this.state.name !== '' || this.state.surname !== '' || this.state.password !== '';

    return(
      <div className={'container'}>
        <main className={classes.main}>
          <CssBaseline />
          <Paper className={classes.paper}>
            <h1 className={'header'}>Admin</h1>
            <div className="district-holder">
              <p className={'title'}>District</p>
              <input
                  className={'district'}
                  type = "text"
                  name = "district"
                  placeholder = "District"
                  onChange={this.handleChange}/>
            </div>

            <div className={'school-holder'}>
              <p className={'title'}>School</p>
              <input
                className={'school'}
                type= "text"
                name= "school"
                placeholder= "School"
                onChange={this.handleChange}/>
                
            </div>

            <div className="name-holder">
              <p className={'title'}>Name</p>
              <input
                  className={'name'}
                  type = "text"
                  name = "name"
                  placeholder = "Name"
                  onChange={this.handleChange}/>
            </div>

            <div className="surname-holder">
              <p className={'title'}>Surname</p>
              <input
                  className={'surname'}
                  type = "text"
                  name = "surname"
                  placeholder = "Surname"
                  onChange={this.handleChange}/>
            </div>

            <div className="password-holder">
              <p className={'title'}>Password</p>
              <input
                  className={'password'}
                  type = "text"
                  name = "password"
                  placeholder = "Password"
                  onChange={this.handleChange}/>
            </div>

            <div className={'submit-holder'}>
              <button
                className={'signIn'}
                type= "button"
                name="SignIn-btn"
                disabled={!isValid}
                style={isValid ? {color: 'white'} : {color: 'lightgrey'}}
                onClick={this.setRedirect}>
                    Sign In
              </button>
            </div>
          </Paper>
        </main>     
      </div>
    )
  }
}

AdminSignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminSignIn);