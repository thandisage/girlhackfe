import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import '../../LearnerSignIn/learner.css';

const styles = theme => ({
    main: {
      width: 'auto',
      height: 100 + '%',
      display: 'block', 
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 1000,
        height: 100 + '%',
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: theme.spacing.unit * 8,
      display: 'flex',
      backgroundColor: 'lightgrey',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
  
  });
class SignUpAdmin extends Component{
    
    constructor(props) {
    super(props);
    this.state={};
    }
    submitSignUp(e) {}

    render() {
        const { classes } = this.props;

        return(
            <div>     
                <main className={classes.main}>
                    <CssBaseline />
                    <Paper className={classes.paper}>
                        <h1 className={'header'}>Admin</h1>
                        <div className="input-group">
                            <p className={'title'}>Name</p>
                            <input
                            className={'name'}
                            type="text"
                            name="text"
                            placeholder="Name"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>Surname</p>
                            <input
                            className={'surname'}
                            type="text"
                            name="text"
                            placeholder="Surname"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>District</p>
                            <input
                            className={'district'}
                            type="text"
                            name="text"
                            placeholder="District"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>School Name</p>
                            <input
                            className={'school'}
                            type="text"
                            name="text"
                            placeholder="SchoolName"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>Email</p>
                            <input
                            className={'email'}
                            type="email"
                            name="email"
                            placeholder="email"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>Password</p>
                            <input
                            className={'password'}
                            type="password"
                            name="password"
                            placeholder="Password"/>
                        </div>

                        <div className="input-group">
                            <p className={'title'}>Confirm Password</p>
                            <input
                            className={'password'}
                            type="password"
                            name="password"
                            placeholder="Password"/>
                        </div>

                        <button
                            className={'signIn'}
                            type="button"
                            name="admin-btn"
                            onClick={this.submitSignUp}>SignUp</button>
                    </Paper>
                </main>
            </div>
        )
    }
};

SignUpAdmin.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(SignUpAdmin);