import React, { Component } from 'react';
import * as ROUTES from '../../constants/routes'
import {Redirect}  from 'react-router-dom';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import '../SignIn/signIn.css';

const styles = theme => ({
    main: {
      width: 'auto',
      height: 100 + '%',
      display: 'block', 
      marginLeft: theme.spacing.unit * 3,
      marginRight: theme.spacing.unit * 3,
      [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
        width: 1000,
        height: 100 + '%',
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    paper: {
      marginTop: 100,
      display: 'flex',
      backgroundColor: 'lightgrey',
      flexDirection: 'column',
      alignItems: 'center',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
  
  });  

class SignUp extends Component {
    constructor(){
        super();
        this.state={
            RedirectA: false,
            RedirectL: false
        }; 
    }
    
   setRedirectA=() =>{
       this.setState({
           RedirectA: true
       })
   } 

   setRedirectL =()=>{
        this.setState({
            RedirectL:true
        })
    }
   
    render(){
        const { classes } = this.props;

        if (this.state.RedirectA){
            return <Redirect to={ROUTES.SIGN_ADMIN}/>}
        if (this.state.RedirectL){
            return<Redirect to={ROUTES.SIGN_LEARNER}/>}  

        return(
            <div className="container">
                <main className={classes.main}>
                <CssBaseline />
                    <Paper className={classes.paper}>
                    <div className={'inner-cont'}>
                        <button
                            className="Admin-btn"
                            type="button"
                            name="Admin-btn"
                            onClick={this.setRedirectA}>
                                Admin
                        </button>
                    
                        <button
                            className="Signup-learner-btn"
                            type="button"
                            name="learner-btn"
                            onClick={this.setRedirectL}>
                                Learner
                        </button>
                    </div>
                    </Paper>
                </main>
        </div>        
        )
    }
};

SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(SignUp);
